--------------------------------------------------------
-- Minetest :: Splash Screen Mod v1.1 (welcome)
--
-- See README.txt for licensing and other information.
-- Copyright (c) 2018-2019, Leslie Ellen Krause
--
-- ./games/minetest_game/mods/welcome/init.lua
--------------------------------------------------------

minetest.register_on_joinplayer( function( player )

	if player:get_hp( ) > 0 then
		local pname = player:get_player_name( )
		local ptime = registry.get_player( pname ).oldtime
		local server_name = minetest.setting_get( "server_name" ) 
		local server_address = minetest.setting_get( "server_address" )
		local formspec = "size[7.8,8.5]"
			.. default.gui_bg
			.. default.gui_bg_img

			.. "box[0.0,0.0;7.5,1.5;#111111]"
			.. string.format( "label[2.0,0.3;%s]", server_name ~= "" and server_name or "Untitled Server" )
			.. string.format( "label[2.0,0.8;%s:%d]", server_address ~= "" and server_address or "localhost", minetest.setting_get( "port" ) )
			.. "image[0.5,0.0;1.5,1.5;inventory_logo.png]"
			.. "button_exit[0.5,7.0;2.8,1;close;Let's Play!]"
			.. string.format( "label[4.5,3.0;%d Players Online]", #registry.player_list )
			.. "textlist[3.5,3.5;4,4.3;player_list;"

		for i, p in pairs( registry.player_list ) do
			formspec = ( i > 1 and formspec .. "," or formspec ) .. p.name ..
				minetest.formspec_escape( ( { "", "", " [STAFF]", " [ADMIN]", " [OWNER]" } )[ p.rank ] )
		end
        	formspec = formspec .. string.format( ";%d;false]", #registry.player_list )
			.. "label[0.0,8.2;For a complete list of available commands, type /help into chat.]"

		if ptime then
			formspec = formspec
				.. string.format( "label[0.0,1.8;Welcome back, %s!]", pname )
				.. string.format( "label[0.0,2.4;You last logged in on %s.]",
					os.date( "%A, %B %d, %Y", ptime ) )
				.. string.format( "image[1.0,3.6;2,4;%s_preview.png]", skins.skins[ pname ] or "character_1" )
		else
			formspec = formspec
				.. string.format( "label[0.0,1.8;Greetings, %s!]", pname )
				.. "label[0.0,2.4;Before starting, please read the rules posted at spawn.]"
				.. "image[1.0,3.6;2,4;character_1_preview.png]"
		end

		minetest.create_form( nil, player, formspec, function( ) end )
	end
end)
