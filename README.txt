Splash Screen Mod v1.0
By Leslie E. Krause

Splash Screen gives multiplayer games that added personal toouch, by greeting players each
time they join, including their last login time and a preview of their skin.

Repository
----------------------

Browse source code...
  https://bitbucket.org/sorcerykid/welcome

Download archive...
  https://bitbucket.org/sorcerykid/welcome/get/master.zip
  https://bitbucket.org/sorcerykid/welcome/get/master.tar.gz

Compatability
----------------------

Minetest 0.4.14+ required

Dependencies
----------------------

Default Mod (required)
  https://github.com/minetest/minetest_game

ActiveFormspecs Mod (required)
  https://bitbucket.org/sorcerykid/formspecs

Player Registry Mod (required)
  https://bitbucket.org/sorcerykid/registry

Simple Skins Mod (required)
  https://notabug.org/TenPlus1/simple_skins

Installation
----------------------

  1) Unzip the archive into the mods directory of your game
  2) Rename the welcome-master directory to "welcome"

License of source code
----------------------------------------------------------

GNU Lesser General Public License v3 (LGPL-3.0)

Copyright (c) 2018-2019, Leslie E. Krause

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU Lesser General Public License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

http://www.gnu.org/licenses/lgpl-2.1.html
